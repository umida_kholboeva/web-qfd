from django.apps import AppConfig


class QfdProjectConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'qfd_project'
