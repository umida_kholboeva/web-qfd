from django.contrib import admin
from .models import *


# Register your models here.
@admin.register(CuttingToolsName)
class CuttingToolsNameAdmin(admin.ModelAdmin):
    list_display = ['cutting_tool_name']


@admin.register(CuttingToolsCharacteristic)
class CuttingToolsCharacteristicAdmin(admin.ModelAdmin):
    list_display = ['cutting_tool_characteristic', 'cutting_tool_name', 'characteristics_unit']


@admin.register(CustomerVoice)
class CustomerVoiceAdmin(admin.ModelAdmin):
    list_display = ['customer_voice', 'project_id']


admin.site.register(Projects)
