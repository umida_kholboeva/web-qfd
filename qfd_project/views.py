from django.shortcuts import render
from .models import *
from django.http import HttpResponseRedirect
from django.views import View


# Create your views here.


def authorization(request):
    return render(request, 'qfd_project/authorization.html')


def registration(request):
    return render(request, 'qfd_project/registration.html')


def main_page(request):
    return render(request, 'qfd_project/main_page.html')


def qfd_team(request):
    return render(request, 'qfd_project/qfd_team.html')


def personal_account(request):
    return render(request, 'qfd_project/personal_account.html')


def data_base(request):
    return render(request, 'qfd_project/data_base.html')


def projects(request):
    return render(request, 'qfd_project/projects_page.html')


def hq_structure(request):
    return render(request, 'qfd_project/hq_structure.html')


# def hq_customer_voice(request):
#     return render(request, 'qfd_project/hq_customer_voice.html')


class HQCustomerVoiceView(View):
    def get(self, request):
        return render(request, 'qfd_project/hq_customer_voice.html')

    def post(self, request):
        if request.method == 'POST':
            characteristics = request.POST.getlist('characteristics[]')
            project_id = request.POST.get('project_id')
            for characteristic in characteristics:
                CustomerVoice.objects.create(customer_voice=characteristic, project_id=project_id)
            return render(request, 'qfd_project/hq_customer_voice.html')


def hq_qfd_team(request):
    return render(request, 'qfd_project/hq_qfd_team.html')


def hq_customer_voice_ranging(request):
    return render(request, 'qfd_project/hq_customer_voice_ranging.html')


def hq_customer_voice_approve(request):
    return render(request, 'qfd_project/hq_customer_voice_approve.html')


def hq_technical_characteristics(request):
    return render(request, 'qfd_project/hq_technical_characteristics.html')


def hq_technical_characteristics_ranging(request):
    return render(request, 'qfd_project/hq_technical_characteristics_ranging.html')


def save_customer_voice(request):
    pass
