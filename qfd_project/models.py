from django.db import models


# Create your models here.

class CuttingToolsName(models.Model):
    cutting_tool_name = models.CharField(max_length=50)

    def __str__(self):
        return self.cutting_tool_name


class CuttingToolsCharacteristic(models.Model):
    cutting_tool_characteristic = models.CharField(max_length=100)
    characteristics_unit = models.CharField(max_length=50)
    cutting_tool_name = models.ForeignKey(CuttingToolsName, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.cutting_tool_characteristic


class Projects(models.Model):
    project_name = models.CharField(max_length=100)

    def __str__(self):
        return self.project_name


class CustomerVoice(models.Model):
    customer_voice = models.CharField(max_length=100)
    project_id = models.ForeignKey(Projects, on_delete=models.CASCADE, null=True, blank=True)

class Users(models.Model):
    login = models.EmailField()
    password = models.CharField(max_length=100)


