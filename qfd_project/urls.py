from django.urls import path
from . import views
from .views import *

urlpatterns = [
    path('', views.authorization, name="authorization"),
    path('registration', views.registration, name="registration"),
    path('main/page', views.main_page, name="main-page"),
    path('qfd/team', views.qfd_team, name="qfd-team"),
    path('personal/account', views.personal_account, name="personal-account"),
    path('data/base', views.data_base, name="data-base"),
    path('projects', views.projects, name="projects-page"),
    path('projects/hq/structure', views.hq_structure, name="hq-structure"),
    path('projects/hq/customer/voice', HQCustomerVoiceView.as_view()),
    path('projects/hq/qfd/team', views.hq_qfd_team),
    path('projects/hq/customer/voice/ranging', views.hq_customer_voice_ranging),
    path('projects/hq/customer/voice/approve', views.hq_customer_voice_approve),
    path('projects/hq/technical/characteristics', views.hq_technical_characteristics),
    path('projects/hq/technical/characteristics/ranging', views.hq_technical_characteristics_ranging),
    path('api', views.save_customer_voice),

]